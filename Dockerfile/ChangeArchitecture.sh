#!/bin/bash
set -e -u -x

DEBFILE=
BUILDINFOFILE=
CHANGESFILE=
FROMARCH=
TARGETARCH=
EXTRADIR=

while getopts ":d:i:c:f:t:e:" opt
do
	case $opt in
		d)
			DEBFILE=$OPTARG
			echo deb file will be: [$DEBFILE]
			;;
		i)
			BUILDINFOFILE=$OPTARG
			echo build info file will be: [$BUILDINFOFILE]
			;;
		c)
			CHANGESFILE=$OPTARG
			echo changes file will be: [$CHANGESFILE]
			;;
		f)
			FROMARCH=$OPTARG
			echo from architecture will be: [$FROMARCH]
			;;
		t)
			TARGETARCH=$OPTARG
			echo target architecture will be: [$TARGETARCH]
			;;
		e)
			EXTRADIR=$OPTARG
			echo extra directory will be: [$EXTRADIR]
			;;
		?)
			echo "there is unrecognized parameter: -$opt"
			exit 1
		;;
	esac
done

rm -rf ${EXTRADIR}

dpkg-deb -X ${DEBFILE} ${EXTRADIR}
dpkg-deb -e ${DEBFILE} ${EXTRADIR}/DEBIAN

sed -i ${EXTRADIR}/DEBIAN/control -e "s/Architecture:.*/Architecture: ${TARGETARCH}/"

OUTPUTDEBFILE=`dpkg-deb -b ${EXTRADIR} .. | cut -d ' ' -f 6 | cut -d \' -f 2`
rm -rf ${EXTRADIR}

OLD_MD5SUM=`md5sum ${DEBFILE} | cut -d ' ' -f 1`
OLD_SHA1SUM=`sha1sum ${DEBFILE} | cut -d ' ' -f 1`
OLD_SHA256SUM=`sha256sum ${DEBFILE} | cut -d ' ' -f 1`
OLD_SIZE=`wc -c < ${DEBFILE}`
OLD_BASENAME=`basename ${DEBFILE}`

NEW_MD5SUM=`md5sum ${OUTPUTDEBFILE} | cut -d ' ' -f 1`
NEW_SHA1SUM=`sha1sum ${OUTPUTDEBFILE} | cut -d ' ' -f 1`
NEW_SHA256SUM=`sha256sum ${OUTPUTDEBFILE} | cut -d ' ' -f 1`
NEW_SIZE=`wc -c < ${OUTPUTDEBFILE}`
NEW_BASENAME=`basename ${OUTPUTDEBFILE}`

OUTPUTBUILDINFOFILE=`echo ${BUILDINFOFILE} | sed "s/${FROMARCH}/${TARGETARCH}/"`

if [ ! -f ${OUTPUTBUILDINFOFILE} ]; then
	cp ${BUILDINFOFILE} ${OUTPUTBUILDINFOFILE}
fi

sed -i ${OUTPUTBUILDINFOFILE} -e "s/ ${OLD_MD5SUM} ${OLD_SIZE} ${OLD_BASENAME}/ ${NEW_MD5SUM} ${NEW_SIZE} ${NEW_BASENAME}/"
sed -i ${OUTPUTBUILDINFOFILE} -e "s/ ${OLD_SHA1SUM} ${OLD_SIZE} ${OLD_BASENAME}/ ${NEW_SHA1SUM} ${NEW_SIZE} ${NEW_BASENAME}/"
sed -i ${OUTPUTBUILDINFOFILE} -e "s/ ${OLD_SHA256SUM} ${OLD_SIZE} ${OLD_BASENAME}/ ${NEW_SHA256SUM} ${NEW_SIZE} ${NEW_BASENAME}/"
sed -i ${OUTPUTBUILDINFOFILE} -e "s/Architecture: ${FROMARCH}/Architecture: ${TARGETARCH}/"
sed -i ${OUTPUTBUILDINFOFILE} -e "s/Build-Architecture: ${FROMARCH}/Build-Architecture: ${TARGETARCH}/"

OUTPUTCHANGESFILE=`echo ${CHANGESFILE} | sed "s/${FROMARCH}/${TARGETARCH}/"`

if [ ! -f ${OUTPUTCHANGESFILE} ]; then
	cp ${CHANGESFILE} ${OUTPUTCHANGESFILE}
fi

sed -i ${OUTPUTCHANGESFILE} -e "s/ ${OLD_MD5SUM} ${OLD_SIZE} \(.*\) ${OLD_BASENAME}/ ${NEW_MD5SUM} ${NEW_SIZE} \1 ${NEW_BASENAME}/"
sed -i ${OUTPUTCHANGESFILE} -e "s/ ${OLD_SHA1SUM} ${OLD_SIZE} ${OLD_BASENAME}/ ${NEW_SHA1SUM} ${NEW_SIZE} ${NEW_BASENAME}/"
sed -i ${OUTPUTCHANGESFILE} -e "s/ ${OLD_SHA256SUM} ${OLD_SIZE} ${OLD_BASENAME}/ ${NEW_SHA256SUM} ${NEW_SIZE} ${NEW_BASENAME}/"
sed -i ${OUTPUTCHANGESFILE} -e "s/Architecture: ${FROMARCH}/Architecture: ${TARGETARCH}/"

echo 1
XXXBUILDINFO=`grep "${FROMARCH}" ${OUTPUTCHANGESFILE} | grep -v "_${FROMARCH}.buildinfo"`&& echo 2
echo 2
echo length of XXXBUILDINFO: [${#XXXBUILDINFO}]
if [ ${#XXXBUILDINFO} -eq 0 ]; then
	OLD_MD5SUM=`md5sum ${BUILDINFOFILE} | cut -d ' ' -f 1`
	OLD_SHA1SUM=`sha1sum ${BUILDINFOFILE} | cut -d ' ' -f 1`
	OLD_SHA256SUM=`sha256sum ${BUILDINFOFILE} | cut -d ' ' -f 1`
	OLD_SIZE=`wc -c < ${BUILDINFOFILE}`
	OLD_BASENAME=`basename ${BUILDINFOFILE}`

	NEW_MD5SUM=`md5sum ${OUTPUTBUILDINFOFILE} | cut -d ' ' -f 1`
	NEW_SHA1SUM=`sha1sum ${OUTPUTBUILDINFOFILE} | cut -d ' ' -f 1`
	NEW_SHA256SUM=`sha256sum ${OUTPUTBUILDINFOFILE} | cut -d ' ' -f 1`
	NEW_SIZE=`wc -c < ${OUTPUTBUILDINFOFILE}`
	NEW_BASENAME=`basename ${OUTPUTBUILDINFOFILE}`

	sed -i ${OUTPUTCHANGESFILE} -e "s/ ${OLD_MD5SUM} ${OLD_SIZE} \(.*\) ${OLD_BASENAME}/ ${NEW_MD5SUM} ${NEW_SIZE} \1 ${NEW_BASENAME}/"
	sed -i ${OUTPUTCHANGESFILE} -e "s/ ${OLD_SHA1SUM} ${OLD_SIZE} ${OLD_BASENAME}/ ${NEW_SHA1SUM} ${NEW_SIZE} ${NEW_BASENAME}/"
	sed -i ${OUTPUTCHANGESFILE} -e "s/ ${OLD_SHA256SUM} ${OLD_SIZE} ${OLD_BASENAME}/ ${NEW_SHA256SUM} ${NEW_SIZE} ${NEW_BASENAME}/"
fi

