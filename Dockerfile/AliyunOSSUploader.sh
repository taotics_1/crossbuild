#!/bin/bash

Host=
Bucket=
AccessKeyId=
AccessKeySecret=

LocalFilePath=
RemotePath=

while getopts ":h:b:k:s:l:r:" opt
do
	case $opt in
		h)
			Host=$OPTARG
			echo "Host will be [$Host]"
			;;
		b)
			Bucket=$OPTARG
			echo "Bucket will be [$Bucket]"
			;;
		k)
			AccessKeyId=$OPTARG
			echo "AccessKeyId will be [$AccessKeyId]"
			;;
		s)
			AccessKeySecret=$OPTARG
			echo "AccessKeySecret will be [$AccessKeySecret]"
			;;
		l)
			LocalFilePath=$OPTARG
			echo "LocalFilePath will be [$LocalFilePath]"
			;;
		r)
			RemotePath=$OPTARG
			echo "RemotePath will be [$RemotePath]"
			;;
		?)
			echo "there is unrecognized parameter: -$opt"
			exit 1
		;;
	esac
done

if [ ${#Host} -eq 0 ]; then
   echo "The Host parameter must be specified"
   exit 1
fi

if [ ${#Bucket} -eq 0 ]; then
   echo "The Bucket parameter must be specified"
   exit 1
fi

if [ ${#AccessKeyId} -eq 0 ]; then
   echo "The AccessKeyId parameter must be specified"
   exit 1
fi

if [ ${#AccessKeySecret} -eq 0 ]; then
   echo "The AccessKeySecret parameter must be specified"
   exit 1
fi

if [ ${#LocalFilePath} -eq 0 ]; then
   echo "The LocalFilePath parameter must be specified"
   exit 1
fi

if [ ${#RemotePath} -eq 0 ]; then
   echo "The RemotePath parameter must be specified"
   exit 1
fi

LocalFileBasename=${LocalFilePath##*/}

OSSHost=${Bucket}.${Host}
OSSResource=/${Bucket}${RemotePath}

OSSContentType=`file -ib ${LocalFilePath} | awk -F ";" '{print $1}'`
OSSDateValue="`TZ=GMT env LC_ALL=en_US.UTF-8 date +'%a, %d %b %Y %H:%M:%S GMT'`"
OSSStringToSign="PUT\n\n${OSSContentType}\n${OSSDateValue}\n${OSSResource}${LocalFileBasename}"
OSSSignature=`echo -en ${OSSStringToSign} | openssl sha1 -hmac ${AccessKeySecret} -binary | base64`

echo
echo "OSSContentType: [${OSSContentType}]"
echo "OSSDateValue: [${OSSDateValue}]"
echo "OSSStringToSign: [${OSSStringToSign}]"
echo "OSSSignature: [${OSSSignature}]"
echo

OSSUrl=http://${OSSHost}${RemotePath}
echo "will upload [${LocalFilePath}] to [${OSSUrl}]"
echo

OSSResponseContent=`curl --retry 5 --retry-delay 2 -i -q -X PUT -T "${LocalFilePath}" \
	-H "Host: ${OSSHost}" \
	-H "Date: ${OSSDateValue}" \
	-H "Content-Type: ${OSSContentType}" \
	-H "Authorization: OSS ${AccessKeyId}:${OSSSignature}" \
	${OSSUrl}`;

echo
echo "upload finished, response content:"
echo ${OSSResponseContent}
